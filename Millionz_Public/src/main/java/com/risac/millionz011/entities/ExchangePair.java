package com.risac.millionz011.entities;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.risac.millionz011.entities.JPAServices.ExchangePairService;
import com.risac.millionz011.entities.repositories.ExchangePairRepository;

@Entity
public class ExchangePair extends PersistedEntity {

	@ManyToOne
	Coin primaryCoin;

	@ManyToOne
	Coin secondaryCoin;

	@ManyToOne
	Exchange exchange;

	public ExchangePair() {
	}

	public ExchangePair(Coin primaryCoin, Coin secondaryCoin, Exchange exchange) {
		super();
		this.primaryCoin = primaryCoin;
		this.secondaryCoin = secondaryCoin;
		this.exchange = exchange;
	}

	public Coin getPrimaryCoin() {
		return primaryCoin;
	}

	public void setPrimaryCoin(Coin primaryCoin) {
		this.primaryCoin = primaryCoin;
	}

	public Coin getSecondaryCoin() {
		return secondaryCoin;
	}

	public void setSecondaryCoin(Coin secondaryCoin) {
		this.secondaryCoin = secondaryCoin;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(exchange, primaryCoin, secondaryCoin);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ExchangePair other = (ExchangePair) obj;
		return Objects.equals(exchange, other.exchange) && Objects.equals(primaryCoin, other.primaryCoin)
				&& Objects.equals(secondaryCoin, other.secondaryCoin);
	}

	@Override
	public String toString() {
		return "ExchangePair [primaryCoin=" + primaryCoin + ", secondaryCoin=" + secondaryCoin + ", exchange="
				+ exchange + "]";
	}

	@Transactional
	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements ExchangePairService {

		@Autowired
		ExchangePairRepository repo;

		@PersistenceContext
		EntityManager em;

		@Override
		public void save(ExchangePair exchangePair) {
			em.persist(exchangePair);
		}

		@Override
		public List<ExchangePair> findAllByPrimaryCoin(Coin coin) {

			return repo.findAllByPrimaryCoin(coin);
		}

		@Override
		public List<ExchangePair> findAllSecondaryByExchange(Exchange exchange) {

			return repo.findAllSecondaryByExchange(exchange);
		}

	}
}
