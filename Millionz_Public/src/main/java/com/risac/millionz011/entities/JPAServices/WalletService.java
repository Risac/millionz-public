package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import com.risac.millionz011.entities.Wallet;

public interface WalletService extends BaseEntityService {

	public void save(Wallet wallet);

	List<Wallet> findAllWallets();

	Wallet findByName(String name);
}
