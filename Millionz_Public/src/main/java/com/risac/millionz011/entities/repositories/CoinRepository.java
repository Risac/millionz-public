package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.risac.millionz011.entities.Coin;

@Repository
public interface CoinRepository extends BaseEntityRepository {

	Coin findBySymbol(String sym);

	Coin save(Coin coin);

	Coin findCoinById(Long id);

	@Query(value = "SELECT * FROM coin c JOIN exchange_coins ec ON c.id = ec.coins_id JOIN exchange e ON e.id = ec.exchange_id WHERE e.name = ?1", nativeQuery = true)
	List<Coin> findAllByExchangeName(String name);

}
