package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.risac.millionz011.entities.Coin;
import com.risac.millionz011.entities.Exchange;
import com.risac.millionz011.entities.ExchangePair;

@Repository
public interface ExchangePairRepository extends BaseEntityRepository {

	ExchangePair save(ExchangePair exchangePair);

	List<ExchangePair> findAllByPrimaryCoin(Coin coin);

	List<ExchangePair> findAllSecondaryByExchange(Exchange exchange);

}
