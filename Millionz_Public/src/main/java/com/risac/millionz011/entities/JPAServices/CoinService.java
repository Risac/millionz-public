package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import com.risac.millionz011.entities.Coin;

public interface CoinService extends BaseEntityService {

	public Coin findBySymbol(String sym);

	public void flush();

	public void save(Coin coin);

	public Coin findCoinById(Long id);

	public void update(Coin coin);

	public List<Coin> findAllByExchangeName(String name);

}
