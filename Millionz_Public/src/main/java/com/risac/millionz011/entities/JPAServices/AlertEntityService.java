package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.risac.millionz011.entities.AlertEntity;

public interface AlertEntityService extends BaseEntityService {

	@Query(value = "select * from alert_entity", nativeQuery = true)
	List<AlertEntity> findAllAlertEntities();

	void save(AlertEntity alertEntity);

	@Modifying
	@Query("delete from AlertEntity where id = :id")
	void deleteById(@Param("id") Long id);

}
