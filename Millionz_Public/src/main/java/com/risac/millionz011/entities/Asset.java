package com.risac.millionz011.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.AssetService;
import com.risac.millionz011.entities.repositories.AssetRepository;

@Entity
public class Asset extends BaseEntity {

	@OneToOne
	private Coin coin;

	@OneToOne
	private Wallet wallet;

	@Column(columnDefinition = "DECIMAL(20,10)")
	private BigDecimal volume;
	private BigDecimal purchasePrice;

	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date purchaseDate;

	@Transient
	private BigDecimal value;

	public Asset() {
	}

	public Asset(Coin coin, BigDecimal volume, BigDecimal purchasePrice, Date purchaseDate, BigDecimal value) {
		super();
		this.coin = coin;
		this.volume = volume;
		this.purchasePrice = purchasePrice;
		this.purchaseDate = purchaseDate;
		this.value = value;
	}

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/*
	 * Calculates total cost of a coin.
	 */
	public BigDecimal getCost() {

		if (purchasePrice == BigDecimal.valueOf(0.00)) {
			return BigDecimal.valueOf(0);
		}
		return volume.multiply(purchasePrice).setScale(2, RoundingMode.HALF_EVEN);

	}

	public String getPurchaseDateString() {
		SimpleDateFormat format = new SimpleDateFormat("dd MMM YYYY");
		String date = format.format(purchaseDate);
		return date;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(coin, purchaseDate, purchasePrice, value, volume, wallet);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Asset other = (Asset) obj;
		return Objects.equals(coin, other.coin) && Objects.equals(purchaseDate, other.purchaseDate)
				&& Objects.equals(purchasePrice, other.purchasePrice) && Objects.equals(value, other.value)
				&& Objects.equals(volume, other.volume) && Objects.equals(wallet, other.wallet);
	}

	@Override
	public String toString() {
		return "OwnedCurrency [currency=" + coin + ", wallet=" + wallet + ", volume=" + volume + ", purchasePrice="
				+ purchasePrice + ", purchaseDate=" + purchaseDate + ", value=" + value + ", getId()=" + getId()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + "]";
	}

	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements AssetService {

		@Autowired
		AssetRepository repo;

		@Autowired
		EntityManager em;

		@SuppressWarnings("unchecked")
		@Override
		public List<Asset> findAllAssets() {
			Query q = em.createNativeQuery("SELECT * FROM asset", Asset.class);

			return q.getResultList();
		}

		@Override
		public List<PersistedEntity> findAll() {
			return repo.findAll();
		}

		@Override
		public void save(Asset asset) {
			repo.save(asset);
		}

		@Override
		public Asset findAssetById(Long id) {
			return repo.findAssetById(id);
		}

		@Override
		public void deleteById(Long id) {
			repo.deleteById(id);

		}

	}

}
