package com.risac.millionz011.entities;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.PersistedEntityService;
import com.risac.millionz011.entities.repositories.PersistedEntityRepository;

@MappedSuperclass
public class PersistedEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	public PersistedEntity() {
	}

	public PersistedEntity(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersistedEntity other = (PersistedEntity) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "PersistedEntity [id=" + id + "]";
	}

	@Transactional
	@Service
	public static class ServiceImpl implements PersistedEntityService {

		@Autowired
		PersistedEntityRepository repo;

		public List<PersistedEntity> findAll() {
			return repo.findAll();
		}

		public Optional<PersistedEntity> findById(Long id) {
			return repo.findById(id);
		}

		public PersistedEntity save(PersistedEntity t) {
			return repo.save(t);
		}

		public void deleteById(Long id) {
			repo.deleteById(id);
		};
	}

}