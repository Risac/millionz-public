package com.risac.millionz011.entities;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.risac.millionz011.entities.JPAServices.CoinService;
import com.risac.millionz011.entities.repositories.CoinRepository;

@Entity
public class Coin extends BaseEntity implements Comparable<Coin> {

	private String symbol;
	private boolean isCurrency;

	@OneToMany
	private List<Exchange> exchange;

	public Coin() {
	}

	public Coin(String symbol, boolean isCurrency, List<Exchange> exchange) {
		super();
		this.symbol = symbol;
		this.isCurrency = isCurrency;
		this.exchange = exchange;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public boolean isCurrency() {
		return isCurrency;
	}

	public void setCurrency(boolean isCurrency) {
		this.isCurrency = isCurrency;
	}

	public List<Exchange> getExchange() {
		return exchange;
	}

	public void setExchange(List<Exchange> exchange) {
		this.exchange = exchange;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(exchange, isCurrency, symbol);
		return result;
	}

	@Override
	public String toString() {
		return "Coin [symbol=" + symbol + ", isCurrency=" + isCurrency + ", exchange=" + exchange + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Coin other = (Coin) obj;
		return Objects.equals(exchange, other.exchange) && isCurrency == other.isCurrency
				&& Objects.equals(symbol, other.symbol);
	}

	@Override
	public int compareTo(Coin coin) {

		return this.getSymbol().compareTo(coin.getSymbol());
	}

	@Transactional
	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements CoinService {

		@Autowired
		EntityManager em;
		@Autowired
		CoinRepository repo;

		// @Transactional(propagation = Propagation.REQUIRED)
		public Coin findBySymbol(String sym) {
			return repo.findBySymbol(sym);
		}

		public void save(Coin t) {
			repo.save(t);
		}

		public void flush() {
			repo.flush();
		}

		@Override
		public Coin findCoinById(Long id) {

			return repo.findCoinById(id);
		}

		@Override
		public void update(Coin coin) {

			em.merge(coin);

		}

		@SuppressWarnings("unchecked")
		@Override
		public List<Coin> findAllByExchangeName(String name) {
			Query q = em.createNativeQuery(
					"SELECT * FROM coin c JOIN exchange_coins ec ON c.id = ec.coins_id JOIN exchange e ON e.id = ec.exchange_id WHERE e.name = ?1",
					Coin.class);
			q.setParameter(1, name);

			return q.getResultList();
		}

	}

}
