package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.risac.millionz011.entities.Wallet;

@Repository
public interface WalletRepository extends BaseEntityRepository {

	public void save(Wallet wallet);

	@Query(value = "SELECT * FROM wallet", nativeQuery = true)
	public List<Wallet> findAllWallets();

	public Wallet findByName(String name);

}
