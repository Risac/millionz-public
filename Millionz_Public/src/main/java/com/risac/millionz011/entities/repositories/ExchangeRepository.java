package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.risac.millionz011.entities.Exchange;

@Repository
public interface ExchangeRepository extends BaseEntityRepository {

	Exchange save(Exchange exchange);

	@Query(value = "SELECT * FROM exchange", nativeQuery = true)
	public List<Exchange> findAllExchanges();

	Exchange findExchangeById(Long id);

	Exchange findExchangeByName(String name);

}
