package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import com.risac.millionz011.entities.Coin;
import com.risac.millionz011.entities.Exchange;
import com.risac.millionz011.entities.ExchangePair;

public interface ExchangePairService extends BaseEntityService {

	void save(ExchangePair exchangePair);

	List<ExchangePair> findAllByPrimaryCoin(Coin coin);

	List<ExchangePair> findAllSecondaryByExchange(Exchange exchange);
}
