package com.risac.millionz011.entities.JPAServices;

import java.util.List;
import java.util.Optional;

import com.risac.millionz011.entities.PersistedEntity;

public interface PersistedEntityService {

	List<PersistedEntity> findAll();

	Optional<PersistedEntity> findById(Long id);

	PersistedEntity save(PersistedEntity t);

	void deleteById(Long id);

}
