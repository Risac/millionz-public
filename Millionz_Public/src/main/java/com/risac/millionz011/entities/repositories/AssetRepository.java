package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.risac.millionz011.entities.Asset;
import com.risac.millionz011.entities.PersistedEntity;

@Repository
public interface AssetRepository extends BaseEntityRepository {

	@Query(value = "select distinct * from coin c join exchange_coins ec on c.id = ec.coins_id join exchange e on e.id = ec.exchange_id", nativeQuery = true)
	List<Asset> findAllAssets();

	List<PersistedEntity> findAll();

	Asset findAssetById(Long id);

	void save(Asset asset);

	@Modifying
	@Query("delete from Asset where id = :id")
	void deleteById(@Param("id") Long id);

}
