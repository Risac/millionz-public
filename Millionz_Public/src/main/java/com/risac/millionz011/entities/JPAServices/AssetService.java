package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.risac.millionz011.entities.Asset;
import com.risac.millionz011.entities.PersistedEntity;

public interface AssetService extends BaseEntityService {

	List<Asset> findAllAssets();

	List<PersistedEntity> findAll();

	Asset findAssetById(Long id);

	void save(Asset asset);

	@Modifying
	@Query("delete from Asset where id = :id")
	void deleteById(@Param("id") Long id);

}
