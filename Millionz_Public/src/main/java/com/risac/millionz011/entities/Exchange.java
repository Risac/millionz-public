package com.risac.millionz011.entities;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.ExchangeService;
import com.risac.millionz011.entities.repositories.ExchangeRepository;

@Entity
public class Exchange extends BaseEntity {

	private String baseURL;

	@ManyToMany(fetch = FetchType.EAGER)
	private List<Coin> coins;

	public Exchange() {
	}

	public Exchange(String name, String baseURL) {
		super(name);
		this.baseURL = baseURL;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public List<Coin> getCoins() {
		return coins;
	}

	public void setCoins(List<Coin> listOfCoins) {
		this.coins = listOfCoins;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(baseURL, coins);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Exchange other = (Exchange) obj;
		return Objects.equals(baseURL, other.baseURL) && Objects.equals(coins, other.coins);
	}

	@Override
	public String toString() {
		return "Exchange [baseURL=" + baseURL + ", currencies=" + coins + ", getName()=" + getName() + ", toString()="
				+ super.toString() + ", getId()=" + getId() + ", getClass()=" + getClass() + "]";
	}

	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements ExchangeService {

		@Autowired
		ExchangeRepository repo;

		@PersistenceContext
		EntityManager em;

		@Override
		public void save(Exchange exchange) {
			repo.save(exchange);

		}

		@SuppressWarnings("unchecked")
		@Override
		public List<Exchange> findAllExchanges() {
			Query q = em.createNativeQuery("SELECT * FROM exchange", Exchange.class);
			return q.getResultList();
		}

		@Override
		public Exchange findExchangeById(Long id) {

			return repo.findExchangeById(id);
		}

		@Override
		public Exchange findExchangeByName(String name) {

			return repo.findExchangeByName(name);
		}

	}

}
