package com.risac.millionz011.entities;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.AlertEntityService;
import com.risac.millionz011.entities.repositories.AlertEntityRepository;

@Entity
public class AlertEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	private Coin coin;

	@OneToOne
	private Exchange exchange;

	private BigDecimal price;
	private Boolean moreThan;

	public AlertEntity() {
	}

	public AlertEntity(Long id, Coin coin, Exchange exchange, BigDecimal price, Boolean moreThan) {
		super();
		this.id = id;
		this.coin = coin;
		this.exchange = exchange;
		this.price = price;
		this.moreThan = moreThan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getMoreThan() {
		return moreThan;
	}

	public void setMoreThan(Boolean moreThan) {
		this.moreThan = moreThan;
	}

	@Override
	public int hashCode() {
		return Objects.hash(coin, exchange, id, moreThan, price);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AlertEntity other = (AlertEntity) obj;
		return Objects.equals(coin, other.coin) && Objects.equals(exchange, other.exchange)
				&& Objects.equals(id, other.id) && Objects.equals(moreThan, other.moreThan)
				&& Objects.equals(price, other.price);
	}

	@Override
	public String toString() {
		return "AlertEntity [id=" + id + ", coin=" + coin + ", exchange=" + exchange + ", price=" + price
				+ ", moreThan=" + moreThan + "]";
	}

	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements AlertEntityService {

		@Autowired
		AlertEntityRepository repo;

		@Autowired
		EntityManager em;

		@SuppressWarnings("unchecked")
		@Override
		public List<AlertEntity> findAllAlertEntities() {
			Query q = em.createNativeQuery("select * from alert_entity", AlertEntity.class);

			return q.getResultList();
		}

		@Override
		public void save(AlertEntity alertEntity) {
			repo.save(alertEntity);

		}

		@Override
		public void deleteById(Long id) {
			repo.deleteById(id);

		}
	}

}
