package com.risac.millionz011.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.WalletService;
import com.risac.millionz011.entities.repositories.WalletRepository;

@Entity
public class Wallet extends BaseEntity {

	public Wallet() {
	}

	public Wallet(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Wallet [getName()=" + getName() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ ", getId()=" + getId() + ", getClass()=" + getClass() + "]";
	}

	@Service
	public static class ServiceImpl extends BaseEntity.ServiceImpl implements WalletService {

		@Autowired
		WalletRepository repo;

		@PersistenceContext
		EntityManager entityManager;

		@Override
		public void save(Wallet wallet) {

			repo.save(wallet);

		}

		@SuppressWarnings("unchecked")
		@Override
		public List<Wallet> findAllWallets() {
			Query q = entityManager.createNativeQuery("SELECT * FROM wallet", Wallet.class);
			return q.getResultList();
		}

		@Override
		public Wallet findByName(String name) {

			return repo.findByName(name);
		}

	}

}
