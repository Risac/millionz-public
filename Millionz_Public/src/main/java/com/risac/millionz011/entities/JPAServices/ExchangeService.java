package com.risac.millionz011.entities.JPAServices;

import java.util.List;

import com.risac.millionz011.entities.Exchange;

public interface ExchangeService extends BaseEntityService {

	public void save(Exchange exchange);

	List<Exchange> findAllExchanges();

	Exchange findExchangeById(Long id);

	Exchange findExchangeByName(String name);

}
