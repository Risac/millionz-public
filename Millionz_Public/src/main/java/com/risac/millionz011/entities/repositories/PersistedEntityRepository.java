package com.risac.millionz011.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.risac.millionz011.entities.PersistedEntity;

public interface PersistedEntityRepository extends JpaRepository<PersistedEntity, Long> {

	List<PersistedEntity> findAll();

	@SuppressWarnings("unchecked")
	PersistedEntity save(PersistedEntity t);

}
