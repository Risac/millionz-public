package com.risac.millionz011.entities;

import java.util.Objects;

import javax.persistence.MappedSuperclass;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.risac.millionz011.entities.JPAServices.BaseEntityService;
import com.risac.millionz011.entities.repositories.BaseEntityRepository;

@MappedSuperclass
public class BaseEntity extends PersistedEntity {

	private String name;

	public BaseEntity() {
	}

	public BaseEntity(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseEntity other = (BaseEntity) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "BaseEntity [name=" + name + ", getId()=" + getId() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + "]";
	}

	@Transactional
	@Service
	public static class ServiceImpl implements BaseEntityService {

		@Autowired
		BaseEntityRepository repo;

//		public BaseEntity findByName(String name) {
//			return repo.findByName(name);
//		}

	}

}
