package com.risac.millionz011.controllers;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.risac.millionz011.entities.AlertEntity;
import com.risac.millionz011.entities.Asset;
import com.risac.millionz011.entities.Coin;
import com.risac.millionz011.entities.Exchange;
import com.risac.millionz011.entities.ExchangePair;
import com.risac.millionz011.entities.Wallet;
import com.risac.millionz011.entities.JPAServices.AlertEntityService;
import com.risac.millionz011.entities.JPAServices.AssetService;
import com.risac.millionz011.entities.JPAServices.CoinService;
import com.risac.millionz011.entities.JPAServices.ExchangePairService;
import com.risac.millionz011.entities.JPAServices.ExchangeService;
import com.risac.millionz011.entities.JPAServices.WalletService;
import com.risac.millionz011.services.PricesDataService;
import com.risac.millionz011.services.json.models.BinanceCoinPrice;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;
import com.risac.millionz011.widgets.alerts.AlertDTO;
import com.risac.millionz011.widgets.calculator.CalculatorApp;
import com.risac.millionz011.widgets.calculator.CoinDTO;
import com.risac.millionz011.widgets.calculator.NewCoinDTO;
import com.risac.millionz011.widgets.chyron.Chyron;
import com.risac.millionz011.widgets.portfolio.Portfolio;
import com.risac.millionz011.widgets.portfolio.PortfolioDTO;

@Controller
public class MainController {

	@Autowired
	private PricesDataService pricesDataService;

	@Autowired
	private CoinService coinServ;

	@Autowired
	private ExchangeService exServ;

	@Autowired
	private ExchangePairService exPairServ;

	@Autowired
	AssetService assetServ;

	@Autowired
	WalletService walletServ;

	@Autowired
	AlertEntityService alertServ;

	private ModelAndView modelAndView;

	private Chyron chyron;
	private List<BittrexCoinPrice> getChyronCoins;

	/*
	 * Maps index.html
	 */
	@GetMapping("/")
	public ModelAndView index() {
		initPage();

		modelAndView.setViewName("index.html");

		return modelAndView;
	}

	/*
	 * Maps alerts.html
	 */
	@RequestMapping("/alerts")
	public ModelAndView alerts(@ModelAttribute("alert") AlertDTO alertDTO) {
		initPage();

		List<Coin> findAllByExchangeName = coinServ.findAllByExchangeName(alertDTO.getExchange());
		Collections.sort(findAllByExchangeName);

		AlertEntity alert = new AlertEntity();

		Coin coin = coinServ.findBySymbol(alertDTO.getSymbol());

		Exchange exchange = exServ.findExchangeByName(alertDTO.getExchange());

		alert.setCoin(coin);
		alert.setExchange(exchange);
		alert.setId(alertDTO.getId());
		alert.setMoreThan(alertDTO.getMoreThan());
		alert.setPrice(alertDTO.getPrice());

		List<AlertEntity> findAllAlertEntities = alertServ.findAllAlertEntities();

		if (alertDTO.getPrice() != null) {
			alertServ.save(alert);
			return new ModelAndView("redirect:/alerts");
		}

		if (alertDTO.getCommand() != null) {
			if (alertDTO.getCommand().equals("deleteMe")) {

				alertServ.deleteById(alert.getId());
				return new ModelAndView("redirect:/alerts");
			}
		}
		modelAndView.addObject("exchange", exchange);
		modelAndView.addObject("findAllSymbolsUnique", findAllByExchangeName);
		modelAndView.addObject("findAll", findAllAlertEntities);
		modelAndView.addObject("alertEntity", alert);
		modelAndView.setViewName("alerts.html");
		return modelAndView;
	}

	/*
	 * Maps add_coin.html
	 */
	@RequestMapping("/add_coin")
	public ModelAndView addCoin(@ModelAttribute("asset") NewCoinDTO dto) {
		initPage();
		Asset asset = new Asset();
		// Add Bittrex coins to Personal Wallet option
		if (dto.getWallet() != null) {
			String exchange = dto.getWallet();

			if (exchange != null && exchange.equals("PERSONAL")) {
				exchange = "BITTREX";

			}
			List<Coin> findAllCoins = coinServ.findAllByExchangeName(exchange);
			Collections.sort(findAllCoins);

			modelAndView.addObject("findAllCoins", findAllCoins);
			modelAndView.addObject("ownedCoin", asset);

		} else

		{

			System.err.println("null!");
			System.err.println(dto);
		}

		// Format coin and save
		if (dto.getVolume() != null) {
			Coin coin = coinServ.findBySymbol(dto.getCoin());
			Wallet wallet = walletServ.findByName(dto.getWallet());

			asset.setCoin(coin);
			asset.setWallet(wallet);
			asset.setName(coin.getName());
			asset.setPurchaseDate(dto.getPurchaseDate());
			asset.setPurchasePrice(dto.getPurchasePrice());
			asset.setVolume(dto.getVolume());

			assetServ.save(asset);

			// return to portfolio page after submission
			return new ModelAndView("redirect:/portfolio");
		}
		return modelAndView;
	}

	/*
	 * Maps edit_coin.html
	 */
	@RequestMapping("/edit_coin")
	public ModelAndView editCoin(PortfolioDTO oldCoin, @ModelAttribute("coin") NewCoinDTO coinDTO) {
		initPage();

		Long id = oldCoin.getId();
		Asset asset = assetServ.findAssetById(id);

		oldCoin.setExchange(asset.getWallet().getName());
		oldCoin.setSymbol(asset.getCoin().getName());
		oldCoin.setVolume(asset.getVolume());
		oldCoin.setPurchasePrice(asset.getPurchasePrice());
		oldCoin.setPurchaseDate(asset.getPurchaseDate());

		String exchange = coinDTO.getWallet();
		if (exchange != null && (exchange.equals("PERSONAL WALLET"))) {
			exchange = "BITTREX";
		}

		List<Coin> findAllByExchangeName = coinServ.findAllByExchangeName(exchange);
		Collections.sort(findAllByExchangeName);

		modelAndView.setViewName("edit_coin.html");
		modelAndView.addObject("oldCoin", oldCoin);
		modelAndView.addObject("findAll", findAllByExchangeName);

		// Transfer data from old coin and save new coin.
		if (coinDTO.getVolume() != null) {
			String coin = coinDTO.getCoin();
			Coin findBySymbol = coinServ.findBySymbol(coin);
			Wallet findByName = walletServ.findByName(coinDTO.getWallet());
			asset.setCoin(findBySymbol);
			asset.setWallet(findByName);
			asset.setVolume(coinDTO.getVolume());
			asset.setPurchasePrice(coinDTO.getPurchasePrice());
			asset.setPurchaseDate(coinDTO.getPurchaseDate());

			assetServ.save(asset);

			// return to portfolio page after submission
			return new ModelAndView("redirect:/portfolio");
		}

		return modelAndView;
	}

	/*
	 * Maps portfolio.html
	 */
	@RequestMapping("/portfolio")
	public ModelAndView checkPortfolio(@ModelAttribute("portfolioDTO") PortfolioDTO portfolioDTO) {
		initPage();
		List<Asset> findAll = assetServ.findAllAssets(); // find all coins in DB

		Portfolio portfolio = new Portfolio();
		List<BigDecimal> calculateAllValues = portfolio.calculateAll(findAll);

		// Calculate all values in DB
		String sumCost = portfolio.sumCost(findAll); // total cost of all owned coins
		String sumValue = portfolio.sumValue(findAll); // total value of all owned coins

		List<BigDecimal> potentialGain = new ArrayList<>();
		List<BigDecimal> potentialProfit = new ArrayList<>();

		if (portfolioDTO.getId() != null && portfolioDTO.getSelling() != null) {
			portfolio.calculatePotentials(findAll, potentialGain, potentialProfit, portfolioDTO);
		} else {
			for (Asset asset : findAll) {
				potentialGain.add(BigDecimal.valueOf(0));
				potentialProfit.add(BigDecimal.valueOf(0));

			}
		}
		// Set Value column.

		for (int i = 0; i < findAll.size(); i++) {
			findAll.get(i).setValue(calculateAllValues.get(i));

		}

		// Delete coin.
		if (portfolioDTO.getCommand() != null && portfolioDTO.getId() != null) {
			if (portfolioDTO.getCommand().equals("deleteMe")) {

				assetServ.deleteById(portfolioDTO.getId());

				return new ModelAndView("redirect:/portfolio");
			}

			// Edit coin.
			if (portfolioDTO.getCommand().equals("editMe")) {

				ModelAndView modelAndView = new ModelAndView("forward:/edit_coin");
				modelAndView.addObject(portfolioDTO);

				return modelAndView;
			}
		}
		// Sort as chosen by client.
		findAll = portfolio.sortPortfolio(portfolioDTO, findAll);

		portfolioDTO.setSelling(null); // reset

		modelAndView.addObject("potentialGain", potentialGain);
		modelAndView.addObject("potentialProfit", potentialProfit);
		modelAndView.addObject("portfolioDTO", portfolioDTO);
		modelAndView.addObject("findAll", findAll);
		modelAndView.addObject("calculateAllValues", calculateAllValues);
		modelAndView.addObject("sumCost", sumCost);
		modelAndView.addObject("sumValue", sumValue);
		modelAndView.setViewName("portfolio.html");

		return modelAndView;
	}

	/*
	 * Maps calculator.html
	 */
	@RequestMapping("/calculator")
	public ModelAndView calculator(@ModelAttribute("coinDTO") CoinDTO coinDTO) {
		initPage();
		CalculatorApp calcApp = new CalculatorApp();

		List<BinanceCoinPrice> binancePrices = pricesDataService.getBinancePrices();
		List<BittrexCoinPrice> bittrexPrices = pricesDataService.getBittrexPrices();

		List<Coin> findAllCoins = coinServ.findAllByExchangeName(coinDTO.getExchangeName());

		Collections.sort(findAllCoins);

		/* Find all currencies for specific coin and exchange. */
		List<Coin> findAllCurrencies = new ArrayList<>();
		Exchange findExchangeByName = exServ.findExchangeByName(coinDTO.getExchangeName());
		Coin findBySymbol = coinServ.findBySymbol(coinDTO.getSymbol());

		/* Search currencies for specific coin */
		List<ExchangePair> findAllByPrimaryCoin = exPairServ.findAllByPrimaryCoin(findBySymbol);

		for (ExchangePair coin : findAllByPrimaryCoin) {
			if (coin.getExchange() == findExchangeByName) {
				findAllCurrencies.add(coin.getSecondaryCoin());
			}
		}
		Collections.sort(findAllCurrencies);

		if (coinDTO.getExchangeName() != null && coinDTO.getSymbol() != null && coinDTO.getVolume() != null) {
			String calculate = calcApp.calculateString(coinDTO, binancePrices, bittrexPrices);
			coinDTO.setResult(calculate);
		}
		modelAndView.addObject("findAllCoins", findAllCoins);
		modelAndView.addObject("findAllCurrencies", findAllCurrencies);

		return modelAndView;
	}

	/*
	 * Add Chyron on every page.
	 */
	private void initPage() {
		modelAndView = new ModelAndView();
		chyron = new Chyron();
		getChyronCoins = chyron.getChyron();

		modelAndView.addObject("getChyron", getChyronCoins);

	}

	/*
	 * Convert Date.
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		// The conversion here should always be in the same
		// format as the string passed in, e.g. 2015-9-9 should be yyyy-MM-dd
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
