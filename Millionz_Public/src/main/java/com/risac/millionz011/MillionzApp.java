package com.risac.millionz011;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.risac.millionz011.entities.AlertEntity;
import com.risac.millionz011.entities.Coin;
import com.risac.millionz011.entities.Exchange;
import com.risac.millionz011.entities.ExchangePair;
import com.risac.millionz011.entities.Wallet;
import com.risac.millionz011.entities.JPAServices.AlertEntityService;
import com.risac.millionz011.entities.JPAServices.CoinService;
import com.risac.millionz011.entities.JPAServices.ExchangePairService;
import com.risac.millionz011.entities.JPAServices.ExchangeService;
import com.risac.millionz011.entities.JPAServices.WalletService;
import com.risac.millionz011.services.CoinsDataService;
import com.risac.millionz011.services.json.models.BinanceCoin;
import com.risac.millionz011.services.json.models.BittrexCoin;
import com.risac.millionz011.widgets.alerts.Alerts;

/**
 * Class MillionzApp is facade pattern for console access to application.
 * 
 * @author damir
 *
 */

@Component
public class MillionzApp {

	@Autowired
	CoinService coinServ;

	@Autowired
	ExchangeService exServ;

	@Autowired
	ExchangePairService pairServ;

	@Autowired
	WalletService walletServ;

	@Autowired
	AlertEntityService alertServ;

	public void go() {

		setup();

		startAlertService();

	}

	public void setup() {

		Exchange bittrexExchange = new Exchange();
		Exchange binanceExchange = new Exchange();

		createExchanges(bittrexExchange, binanceExchange);

		setupBittrex(bittrexExchange);

		setupBinance(binanceExchange);

		setupWallets();

		System.err.println("Saving coins and exchanges complete.");
	}

	private void createExchanges(Exchange bittrexExchange, Exchange binanceExchange) {

		bittrexExchange.setName("BITTREX");
		bittrexExchange.setBaseURL("https://api.bittrex.com");

		binanceExchange.setName("BINANCE");
		binanceExchange.setBaseURL("https://api.binance.com");
	}

	private void setupBittrex(Exchange bittrexExchange) {
		System.err.println("STARTING BITTREX SETUP");
		CoinsDataService dataServ = new CoinsDataService();

		List<BittrexCoin> coins = dataServ.getBittrexCoins();

		List<Coin> listOfCoins = new ArrayList<>();

		// Save Bittrex Coins to DB
		for (BittrexCoin bittrexCoin : coins) {

			Coin findCoin = coinServ.findBySymbol(bittrexCoin.getSymbol());

			if (findCoin == null) {
				Coin coin = new Coin();
				coin.setName(bittrexCoin.getName());
				coin.setSymbol(bittrexCoin.getSymbol());
				listOfCoins.add(coin);
				coinServ.save(coin);
			}

		}

		// Save Bittrex currencies as coins
		for (BittrexCoin bittrexCoin : coins) {
			Coin findCurrency = coinServ.findBySymbol(bittrexCoin.getTradeAsset());
			Coin coin = new Coin();

			if (findCurrency == null) {
				coin.setName(coin.getSymbol());
				coin.setSymbol(bittrexCoin.getTradeAsset());
				coin.setCurrency(true);

				listOfCoins.add(coin);

				coinServ.save(coin);

			} else if (findCurrency != null) {

				Coin findCoinById = coinServ.findCoinById(findCurrency.getId());
				findCoinById.setCurrency(true);

				coinServ.update(findCoinById);

			}

		}

		// Save Bittrex Exchange to DB
		bittrexExchange.setCoins(listOfCoins);

		Exchange findExchangeByName = exServ.findExchangeByName(bittrexExchange.getName());

		// Save Bittrex pairs
		for (BittrexCoin bittrexCoin : coins) {
			ExchangePair exchangePair = new ExchangePair();

			Coin primary = coinServ.findBySymbol(bittrexCoin.getSymbol());
			Coin secondary = coinServ.findBySymbol(bittrexCoin.getTradeAsset());

			exchangePair.setExchange(bittrexExchange);
			exchangePair.setPrimaryCoin(primary);
			exchangePair.setSecondaryCoin(secondary);

			if (findExchangeByName == null) {
				exServ.save(bittrexExchange);
				pairServ.save(exchangePair);
			}

		}

	}

	public void setupBinance(Exchange binanceExchange) {
		System.err.println("STARTING BINANCE SETUP");
		CoinsDataService dataServ = new CoinsDataService();

		List<BinanceCoin> binanceCoins = dataServ.getBinanceCoins();

		List<Coin> listOfCoins = new ArrayList<>();

		// Save Binance Coins to DB
		for (BinanceCoin binanceCoin : binanceCoins) {

			Coin findCoin = coinServ.findBySymbol(binanceCoin.getSymbol());

			if (findCoin == null) {

				Coin coin = new Coin();
				coin.setSymbol(binanceCoin.getSymbol());
				coin.setName(coin.getSymbol());

				listOfCoins.add(coin);

				coinServ.save(coin);

			}

			if (findCoin != null) {
				boolean x = false;

				for (Coin coin : listOfCoins) {

					if (coin.getSymbol().equals(findCoin.getSymbol())) {
						x = true;
						break;
					}
				}

				if (x == false) {
					listOfCoins.add(findCoin);
				}
			}
		}

		// Save Binance currencies as coins
		for (

		BinanceCoin binanceCoin : binanceCoins) {
			Coin findCurrency = coinServ.findBySymbol(binanceCoin.getTradeAsset());
			Coin coin = new Coin();

			if (findCurrency == null) {
				// coin.setName(coin.getSymbol());

				coin.setSymbol(binanceCoin.getTradeAsset());
				coin.setCurrency(true);

				listOfCoins.add(coin);

				coinServ.save(coin);

			} else if (findCurrency != null) {

				Coin findCoinById = coinServ.findCoinById(findCurrency.getId());
				findCoinById.setCurrency(true);
				findCoinById.setName(findCoinById.getName());

				coinServ.update(findCoinById);

			}

		}

		// Save Bittrex Exchange to DB
		binanceExchange.setCoins(listOfCoins);

		Exchange findExchangeByName1 = exServ.findExchangeByName(binanceExchange.getName());

		// Save Bittrex pairs
		for (BinanceCoin binanceCoin : binanceCoins) {
			ExchangePair exchangePair = new ExchangePair();

			Coin primary = coinServ.findBySymbol(binanceCoin.getSymbol());
			Coin secondary = coinServ.findBySymbol(binanceCoin.getTradeAsset());

			exchangePair.setExchange(binanceExchange);
			exchangePair.setPrimaryCoin(primary);
			exchangePair.setSecondaryCoin(secondary);

			if (findExchangeByName1 == null) {
				exServ.save(binanceExchange);
				pairServ.save(exchangePair);
			}

		}

	}

	public void setupWallets() {
		System.err.println("STARTING WALLETS SETUP");

		String[] wallets = { "BITTREX", "BINANCE", "PERSONAL" };

		for (int i = 0; i < wallets.length; i++) {
			Wallet wallet = new Wallet();
			wallet.setName(wallets[i]);

			Wallet findByName = walletServ.findByName(wallet.getName());

			if (findByName == null) {
				walletServ.save(wallet);
			}
		}
	}

	/**
	 * Starts Alert checker.
	 */
	private void startAlertService() {

		Alerts alert = new Alerts();

		// Convert to lambda
		Runnable checkAlerts = () -> {
			// get the latest alerts.
			List<AlertEntity> findAll = alertServ.findAllAlertEntities();

			// Delete all fulfilled alerts.
			List<Long> init = alert.init(findAll);
			for (Long id : init) {

				alertServ.deleteById(id);
			}
		};

		// Schedule alert checker refresh.
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(checkAlerts, 0, 1, TimeUnit.MINUTES);
		System.err.println(Thread.currentThread().getName());
	}

}