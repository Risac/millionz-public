package com.risac.millionz011.widgets.portfolio;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Controller;

import com.risac.millionz011.entities.Asset;
import com.risac.millionz011.services.PricesDataService;
import com.risac.millionz011.services.json.models.BinanceCoinPrice;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;
import com.risac.millionz011.widgets.calculator.CalculatorApp;
import com.risac.millionz011.widgets.calculator.CoinDTO;

@Controller
public class Portfolio {

	public List<BigDecimal> calculateAll(List<Asset> findAll) {

		CoinDTO coinDTO = new CoinDTO();

		PricesDataService priceServ = new PricesDataService();

		List<BinanceCoinPrice> binanceData = priceServ.getBinancePrices();
		List<BittrexCoinPrice> bittrexData = priceServ.getBittrexData();

		List<BigDecimal> listResults = new ArrayList<>();
		CalculatorApp calc = new CalculatorApp();

		for (Asset asset : findAll) {

			coinDTO.setVolume(asset.getVolume());
			coinDTO.setExchangeName(asset.getWallet().getName());
			coinDTO.setSymbol(asset.getCoin().getSymbol());

			coinDTO.setTradeablePair("USDT");

			BigDecimal calculateResult = calc.calculate(coinDTO, binanceData, bittrexData);

			if (calculateResult != null)
				listResults.add(calculateResult.setScale(2, RoundingMode.HALF_EVEN));
		}

		return listResults;
	}

	public String sumCost(List<Asset> findAll) {
		DecimalFormat df = new DecimalFormat("#,###.00");
		double sum = 0;

		for (Asset ownedCoin : findAll) {
			sum = sum + ownedCoin.getCost().doubleValue();
		}

		return "Total cost is: " + df.format(sum) + " USDT.";
	}

	public String sumValue(List<Asset> findAll) {

		DecimalFormat df = new DecimalFormat("#,###.00");

		List<BigDecimal> calculateAll = calculateAll(findAll);

		BigDecimal sumBittrex = BigDecimal.valueOf(0);
		BigDecimal sumBinance = BigDecimal.valueOf(0);
		BigDecimal sumWallet = BigDecimal.valueOf(0);

		if (findAll.size() > 0) {
			for (int i = 0; i < calculateAll.size(); i++) {

				String wallet = findAll.get(i).getWallet().getName();

				if (wallet.equals("BITTREX")) {
					sumBittrex = sumBittrex.add(calculateAll.get(i));

				}
				if (wallet.equals("BINANCE")) {
					sumBinance = sumBinance.add(calculateAll.get(i));

				}
				if (wallet.equals("PERSONAL")) {
					sumWallet = sumWallet.add(calculateAll.get(i));

				}

			}
		}

		String result = "Total value on Bittrex is: " + df.format(sumBittrex) + " USDT."
				+ " Total value on Binance is: " + df.format(sumBinance) + " USDT."
				+ " Total value in Your Personal Wallet is: " + df.format(sumWallet) + " USDT.";

		return result;

	}

	/*
	 * Calculates values of Potential Gain and Potential Profit based on target sale
	 * price in portfolioDTO.
	 */
	public void calculatePotentials(List<Asset> findAll, List<BigDecimal> potentialGain,
			List<BigDecimal> potentialProfit, PortfolioDTO portfolioDTO) {

		for (Asset ownedCoin : findAll) {

			if (portfolioDTO.getId() != null)
				if (portfolioDTO.getId().equals(ownedCoin.getId())) {

					BigDecimal multiply = ownedCoin.getVolume().multiply(portfolioDTO.getSelling());

					potentialGain.add(multiply.setScale(2, RoundingMode.HALF_EVEN));
					potentialProfit.add(multiply.subtract(ownedCoin.getCost()).setScale(2, RoundingMode.HALF_EVEN));
				} else {

					potentialGain.add(BigDecimal.valueOf(0));
					potentialProfit.add(BigDecimal.valueOf(0));
				}
		}
	}

	public List<Asset> sortPortfolio(PortfolioDTO portfolioDTO, List<Asset> findAll) {

		if (portfolioDTO.getCommand() != null) {

			if (portfolioDTO.getCommand().equals("ExchangeAsc")) {
				return findAll.stream().sorted(
						(asset1, asset2) -> asset1.getWallet().getName().compareTo(asset2.getWallet().getName()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("ExchangeDesc")) {
				return findAll.stream().sorted(
						(asset1, asset2) -> asset2.getWallet().getName().compareTo(asset1.getWallet().getName()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("SymbolAsc")) {
				return findAll.stream().sorted(
						(asset1, asset2) -> asset1.getCoin().getSymbol().compareTo(asset2.getCoin().getSymbol()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("SymbolDesc")) {
				return findAll.stream().sorted(
						(asset1, asset2) -> asset2.getCoin().getSymbol().compareTo(asset1.getCoin().getSymbol()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("QuantityAsc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset1.getVolume().compareTo(asset2.getVolume()))
						.toList();

			}

			else if (portfolioDTO.getCommand().equals("QuantityDesc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset2.getVolume().compareTo(asset1.getVolume()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("DateAsc")) {
				return findAll.stream().sorted(Comparator.comparing(Asset::getPurchaseDate)).toList();

			}

			else if (portfolioDTO.getCommand().equals("DateDesc")) {
				return findAll.stream().sorted(Comparator.comparing(Asset::getPurchaseDate).reversed()).toList();
			}

			else if (portfolioDTO.getCommand().equals("PriceAsc")) {
				return findAll.stream()
						.sorted((asset1, asset2) -> asset1.getPurchasePrice().compareTo(asset2.getPurchasePrice()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("PriceDesc")) {
				return findAll.stream()
						.sorted((asset1, asset2) -> asset2.getPurchasePrice().compareTo(asset1.getPurchasePrice()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("CostAsc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset1.getCost().compareTo(asset2.getCost()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("CostDesc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset2.getCost().compareTo(asset1.getCost()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("ValueAsc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset1.getValue().compareTo(asset2.getValue()))
						.toList();
			}

			else if (portfolioDTO.getCommand().equals("ValueDesc")) {
				return findAll.stream().sorted((asset1, asset2) -> asset2.getValue().compareTo(asset1.getValue()))
						.toList();
			}

		}
		return findAll;
	}

}