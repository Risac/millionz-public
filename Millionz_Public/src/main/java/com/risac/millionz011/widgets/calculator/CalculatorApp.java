package com.risac.millionz011.widgets.calculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.stereotype.Component;

import com.risac.millionz011.services.json.models.BinanceCoinPrice;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;

@Component
public class CalculatorApp {

	// Turn result into String for view.
	public String calculateString(CoinDTO coinDTO, List<BinanceCoinPrice> binancePrices,
			List<BittrexCoinPrice> bittrexPrices) {
		DecimalFormat df = new DecimalFormat("#,###.00");
		String resultString = null;

		BigDecimal calculatorResult = calculate(coinDTO, binancePrices, bittrexPrices);
		if (coinDTO.getTradeablePair() != null && calculatorResult != null) {
			if (coinDTO.getTradeablePair().contains("USD") || coinDTO.getTradeablePair().contains("EUR")
					|| coinDTO.getTradeablePair().contains("AUD") || coinDTO.getTradeablePair().contains("GBP")
					|| coinDTO.getTradeablePair().contains("NGN") || coinDTO.getTradeablePair().contains("PAX")
					|| coinDTO.getTradeablePair().contains("RUB") || coinDTO.getTradeablePair().contains("ZAR")) {

				resultString = "The value of your " + coinDTO.getSymbol() + " on " + coinDTO.getExchangeName() + " is: "
						+ df.format(calculatorResult) + " " + coinDTO.getTradeablePair();

			} else {
				resultString = "The value of your " + coinDTO.getSymbol() + " on " + coinDTO.getExchangeName() + " is: "
						+ calculatorResult + " " + coinDTO.getTradeablePair();
			}
		}
		return resultString;
	}

	// Calculate volume * price, return BigDecimal result.
	public BigDecimal calculate(CoinDTO coinDTO, List<BinanceCoinPrice> binancePrices,
			List<BittrexCoinPrice> bittrexPrices) {
		BigDecimal multiply = null;

		BigDecimal price = findPrice(coinDTO, binancePrices, bittrexPrices);

		if (price != null) {
			multiply = price.multiply(coinDTO.getVolume());
		}

		return multiply;
	}

	// Return price from List of prices from exchanges
	private BigDecimal findPrice(CoinDTO coinDTO, List<BinanceCoinPrice> binancePrices,
			List<BittrexCoinPrice> bittrexPrices) {

		BigDecimal price = null;
		String exchangeName = coinDTO.getExchangeName();

		if (exchangeName.equals("PERSONAL")) {
			exchangeName = "BITTREX";
		}

		if (exchangeName.equals("BITTREX")) {

			String pair = coinDTO.getTradeablePair() + "-" + coinDTO.getSymbol();

			for (BittrexCoinPrice bittrexCoinPrice : bittrexPrices) {
				if (pair.equals(bittrexCoinPrice.getMarketName())) {

					price = bittrexCoinPrice.getLast();
					return price;
				}

			}

		}

		if (exchangeName.equals("BINANCE")) {

			String pair = coinDTO.getSymbol() + coinDTO.getTradeablePair();

			for (BinanceCoinPrice binanceCoinPrice : binancePrices) {

				if (pair.equals(binanceCoinPrice.getSymbol())) {
					price = binanceCoinPrice.getPrice();

					return price;

				}

			}
		}
		return price;
	}

}
