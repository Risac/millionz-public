package com.risac.millionz011.widgets.portfolio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import org.springframework.stereotype.Component;

@Component
public class PortfolioDTO {

	private String exchange;
	private String symbol;
	private BigDecimal volume;
	private BigDecimal purchasePrice;
	private BigDecimal selling;
	private Date purchaseDate;

	private Long id;

	private String command;

	public PortfolioDTO() {
	}

	public PortfolioDTO(String exchange, String symbol, BigDecimal volume, BigDecimal purchasePrice, BigDecimal selling,
			Date purchaseDate, Long id, String command) {
		super();
		this.exchange = exchange;
		this.symbol = symbol;
		this.volume = volume;
		this.purchasePrice = purchasePrice;
		this.selling = selling;
		this.purchaseDate = purchaseDate;
		this.id = id;
		this.command = command;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getSelling() {
		return selling;
	}

	public void setSelling(BigDecimal selling) {
		this.selling = selling;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public int hashCode() {
		return Objects.hash(command, exchange, id, purchaseDate, purchasePrice, selling, symbol, volume);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PortfolioDTO other = (PortfolioDTO) obj;
		return Objects.equals(command, other.command) && Objects.equals(exchange, other.exchange)
				&& Objects.equals(id, other.id) && Objects.equals(purchaseDate, other.purchaseDate)
				&& Objects.equals(purchasePrice, other.purchasePrice) && Objects.equals(selling, other.selling)
				&& Objects.equals(symbol, other.symbol) && Objects.equals(volume, other.volume);
	}

	@Override
	public String toString() {
		return "PortfolioDTO [exchange=" + exchange + ", symbol=" + symbol + ", volume=" + volume + ", purchasePrice="
				+ purchasePrice + ", selling=" + selling + ", purchaseDate=" + purchaseDate + ", id=" + id
				+ ", command=" + command + "]";
	}

}