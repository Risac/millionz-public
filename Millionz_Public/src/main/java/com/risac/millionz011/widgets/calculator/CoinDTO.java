package com.risac.millionz011.widgets.calculator;

import java.math.BigDecimal;
import java.util.Objects;

import org.springframework.stereotype.Component;

@Component
public class CoinDTO {

	private BigDecimal volume;
	private String result;
	private String symbol;
	private String tradeablePair;
	private String exchangeName;

	public CoinDTO() {
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getTradeablePair() {
		return tradeablePair;
	}

	public void setTradeablePair(String tradeablePair) {
		this.tradeablePair = tradeablePair;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(exchangeName, result, symbol, tradeablePair, volume);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CoinDTO other = (CoinDTO) obj;
		return Objects.equals(exchangeName, other.exchangeName) && Objects.equals(result, other.result)
				&& Objects.equals(symbol, other.symbol) && Objects.equals(tradeablePair, other.tradeablePair)
				&& Objects.equals(volume, other.volume);
	}

	@Override
	public String toString() {
		return "CoinDTO [volume=" + volume + ", result=" + result + ", symbol=" + symbol + ", tradeablePair="
				+ tradeablePair + ", exchangeName=" + exchangeName + "]";
	}

}
