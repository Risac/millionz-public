package com.risac.millionz011.widgets.chyron;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.risac.millionz011.services.PricesDataService;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;

//TODO better way to sort List
public class Chyron {

	PricesDataService priceServ = new PricesDataService();

	public List<BittrexCoinPrice> getChyron() {
		List<BittrexCoinPrice> selected = new ArrayList<>();
		List<BittrexCoinPrice> bittrexCoin = priceServ.getBittrexData(); // Use Bittrex API for prices

		BittrexCoinPrice[] sortedArray = new BittrexCoinPrice[9];

		// Change name and custom sort coins
		for (BittrexCoinPrice sum : bittrexCoin) {

			if (sum.getMarketName().equals("USDT-BTC")) {
				sum.setMarketName("Bitcoin");
				sortedArray[0] = sum;
			}
			if (sum.getMarketName().equals("USDT-ETH")) {
				sum.setMarketName("Ethereum");
				sortedArray[1] = sum;
			}
			if (sum.getMarketName().equals("USDT-ADA")) {
				sum.setMarketName("Cardano");
				sortedArray[2] = sum;
			}
			if (sum.getMarketName().equals("USDT-DOT")) {
				sum.setMarketName("Polkadot");
				sortedArray[3] = sum;
			}
			if (sum.getMarketName().equals("USDT-LINK")) {
				sum.setMarketName("ChainLink");
				sortedArray[4] = sum;
			}
			if (sum.getMarketName().equals("USDT-LTC")) {
				sum.setMarketName("Litecoin");
				sortedArray[5] = sum;
			}
			if (sum.getMarketName().equals("USDT-ENJ")) {
				sum.setMarketName("Enjin");
				sortedArray[6] = sum;
			}
			if (sum.getMarketName().equals("USDT-XRP")) {
				sum.setMarketName("Ripple");
				sortedArray[7] = sum;
			}
			if (sum.getMarketName().equals("USDT-DOGE")) {
				sum.setMarketName("Dogecoin");
				sortedArray[8] = sum;
			}

		}

		// Change to List
		for (int i = 0; i < sortedArray.length; i++) {
			selected.add(sortedArray[i]);
		}

		calculatePercent(selected);

		return selected;
	}

	/**
	 * Calculate percent of change using of List <BittrexCoinPrice>.
	 */
	private List<BittrexCoinPrice> calculatePercent(List<BittrexCoinPrice> selected) {
		List<BittrexCoinPrice> calculated = new ArrayList<>();

		for (BittrexCoinPrice sum : selected) {

			/* Calculate */
			BigDecimal last = sum.getLast();
			BigDecimal prevDay = sum.getPrevDay();

			// Use formula (V2-V1)/V1 * 100
			BigDecimal compare = last.subtract(prevDay);
			BigDecimal divide = compare.divide(last, 4, RoundingMode.HALF_EVEN);
			BigDecimal multiply = divide.multiply(BigDecimal.valueOf(100)).setScale(2);

			// Set in object
			sum.setPrevDay(multiply);

			sum.setLast(last.setScale(2, RoundingMode.HALF_EVEN));
			calculated.add(sum);
		}
		return calculated;
	}
}
