package com.risac.millionz011.widgets.alerts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.risac.millionz011.entities.AlertEntity;
import com.risac.millionz011.services.EmailService;
import com.risac.millionz011.services.PricesDataService;
import com.risac.millionz011.services.json.models.BinanceCoinPrice;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;
import com.risac.millionz011.widgets.calculator.CoinDTO;

public class Alerts {
	/**
	 * Method init() changes alertEntity to coinDTO, checks prices from exchanges,
	 * prepares message for sending, send emails and provides a list of deleteable
	 * alertEntities.
	 */
	public List<Long> init(List<AlertEntity> findAll) {

		EmailService emServ = new EmailService();

		List<Long> deleteMe = new ArrayList<>();
		PricesDataService priceServ = new PricesDataService();

		List<BinanceCoinPrice> binanceData = priceServ.getBinancePrices();
		List<BittrexCoinPrice> bittrexData = priceServ.getBittrexPrices();

		if (findAll != null) {
			for (AlertEntity alertEntity : findAll) {

				// change to coinDTO
				CoinDTO coinDTO = new CoinDTO();

				coinDTO.setExchangeName(alertEntity.getExchange().getName());
				coinDTO.setSymbol(alertEntity.getCoin().getSymbol());
				coinDTO.setTradeablePair("USDT");
				coinDTO.setVolume(BigDecimal.valueOf(1));

				// Check price

				BigDecimal price = priceServ.getPrice(coinDTO, binanceData, bittrexData);

				if (alertEntity.getMoreThan()) {

					if (alertEntity.getPrice().compareTo(price) < 0) {

						String setMessage = emServ.setMessage(alertEntity);

						deleteMe.add(alertEntity.getId());

						emServ.sendMail(setMessage);
					}

				} else if (!alertEntity.getMoreThan()) {

					if (alertEntity.getPrice().compareTo(price) > 0) {
						String setMessage = emServ.setMessage(alertEntity);

						deleteMe.add(alertEntity.getId());

						emServ.sendMail(setMessage);
					}
				}
			}

		}

		return deleteMe;
	}

}