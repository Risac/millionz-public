package com.risac.millionz011.widgets.calculator;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class NewCoinDTO {

	private String coin;
	private String wallet;
	private BigDecimal volume;
	private BigDecimal purchasePrice;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date purchaseDate;
	private BigDecimal value;

	public NewCoinDTO() {
	}

	public NewCoinDTO(String coin, String wallet, BigDecimal volume, BigDecimal purchasePrice, Date purchaseDate,
			BigDecimal value) {
		super();
		this.coin = coin;
		this.wallet = wallet;
		this.volume = volume;
		this.purchasePrice = purchasePrice;
		this.purchaseDate = purchaseDate;
		this.value = value;
	}

	public String getCoin() {
		return coin;
	}

	public void setCoin(String coin) {
		this.coin = coin;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "NewCoinDTO [coin=" + coin + ", wallet=" + wallet + ", volume=" + volume + ", purchasePrice="
				+ purchasePrice + ", purchaseDate=" + purchaseDate + ", value=" + value + "]";
	}

}
