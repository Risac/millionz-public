package com.risac.millionz011.widgets.alerts;

import java.math.BigDecimal;
import java.util.Objects;

public class AlertDTO {

	private Long id;
	private String symbol;
	private String exchange;
	private BigDecimal price;
	private Boolean moreThan;
	private String command;

	public AlertDTO(Long id, String symbol, String exchange, BigDecimal price, Boolean moreThan, String command) {
		super();
		this.id = id;
		this.symbol = symbol;
		this.exchange = exchange;
		this.price = price;
		this.moreThan = moreThan;
		this.command = command;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getMoreThan() {
		return moreThan;
	}

	public void setMoreThan(Boolean moreThan) {
		this.moreThan = moreThan;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public int hashCode() {
		return Objects.hash(command, exchange, id, moreThan, price, symbol);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AlertDTO other = (AlertDTO) obj;
		return Objects.equals(command, other.command) && Objects.equals(exchange, other.exchange)
				&& Objects.equals(id, other.id) && Objects.equals(moreThan, other.moreThan)
				&& Objects.equals(price, other.price) && Objects.equals(symbol, other.symbol);
	}

	@Override
	public String toString() {
		return "AlertDTO [id=" + id + ", symbol=" + symbol + ", exchange=" + exchange + ", price=" + price
				+ ", moreThan=" + moreThan + ", command=" + command + "]";
	}

}
