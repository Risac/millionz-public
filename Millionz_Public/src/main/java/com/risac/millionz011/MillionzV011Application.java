package com.risac.millionz011;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EntityScan(basePackages = { "com.risac.millionz011" })
public class MillionzV011Application {

	private static final Logger log = LoggerFactory.getLogger(MillionzV011Application.class);

	public static void main(String[] args) {
		SpringApplication.run(MillionzV011Application.class, args);

	}

	@Bean
	public CommandLineRunner demo(MillionzApp app) {
		return (args) -> {
			app.go();
		};
	}

}
