package com.risac.millionz011.services.json;

import java.util.List;
import java.util.Objects;

import com.risac.millionz011.services.json.models.BittrexCoin;

/**
 * Class BittrexSymbolJson is the model used for processing Bittrex API JSON
 * (symbols) fields.
 */
public class BittrexCoinsJson {

	private boolean success;
	private String message;
	private List<BittrexCoin> result;

	public BittrexCoinsJson(boolean success, String message, List<BittrexCoin> result) {
		super();
		this.success = success;
		this.message = message;
		this.result = result;
	}

	public List<BittrexCoin> getResult() {
		return result;
	}

	public void setResult(List<BittrexCoin> result) {
		this.result = result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(result);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BittrexCoinsJson other = (BittrexCoinsJson) obj;
		return Objects.equals(result, other.result);
	}

	@Override
	public String toString() {
		return "BittrexCoinJson [success=" + success + ", message=" + message + ", result=" + result + "]";
	}

}
