package com.risac.millionz011.services.json;

import java.util.List;

import com.risac.millionz011.services.json.models.BinanceCoinPrice;

/**
 * Class BinancePriceTickerJson is the model used for processing Binance API
 * (ticker) JSON.
 */
public class BinancePriceTickerJson {

	private List<BinanceCoinPrice> result;

	public BinancePriceTickerJson(List<BinanceCoinPrice> result) {
		this.result = result;
	}

	public List<BinanceCoinPrice> getResult() {
		return result;
	}

	public void setResult(List<BinanceCoinPrice> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "BinanceTicker [result=" + result + "]";
	}

}
