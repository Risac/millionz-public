package com.risac.millionz011.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.risac.millionz011.services.json.BittrexPriceTickerJson;
import com.risac.millionz011.services.json.models.BinanceCoinPrice;
import com.risac.millionz011.services.json.models.BittrexCoinPrice;
import com.risac.millionz011.widgets.calculator.CoinDTO;

@Component
public class PricesDataService {

	/**
	 * Get data from Bittrex exchange.
	 */
	public List<BittrexCoinPrice> getBittrexPrices() {

		String URL = "https://api.bittrex.com/api/v1.1/public/getmarketsummaries";
		List<BittrexPriceTickerJson> coins = null;
		List<BittrexCoinPrice> results = new ArrayList<>();
		Gson gson = new Gson();

		try {
			URL url = new URL(URL);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			coins = Arrays.asList(gson.fromJson(bufferedReader, BittrexPriceTickerJson.class));

			for (BittrexPriceTickerJson sym : coins) {
				results = sym.getResult();
			}
			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * Get data from Binance exchange.
	 */
	public List<BinanceCoinPrice> getBinancePrices() {

		String URL = "https://api.binance.com/api/v3/ticker/price";

		ArrayList<BinanceCoinPrice> coins = null;

		Gson gson = new Gson();

		try {
			URL url = new URL(URL);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			Type userListType = new TypeToken<ArrayList<BinanceCoinPrice>>() {
			}.getType();

			coins = gson.fromJson(bufferedReader, userListType);

			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return coins;
	}

	public List<BittrexCoinPrice> getBittrexData() {

		String URL = "https://api.bittrex.com/api/v1.1/public/getmarketsummaries";
		List<BittrexPriceTickerJson> coins = null;
		List<BittrexCoinPrice> results = new ArrayList<>();
		Gson gson = new Gson();

		try {
			URL url = new URL(URL);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			coins = Arrays.asList(gson.fromJson(bufferedReader, BittrexPriceTickerJson.class));

			for (BittrexPriceTickerJson sym : coins) {
				results = sym.getResult();
			}
			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	public BigDecimal getPrice(CoinDTO coinDTO, List<BinanceCoinPrice> binanceData,
			List<BittrexCoinPrice> bittrexData) {

		BigDecimal price = null;

		// Select exchange
		String exchange = coinDTO.getExchangeName();

		if (exchange == null || coinDTO.getTradeablePair() == null || coinDTO.getVolume() == null) {
			return BigDecimal.ZERO;
		}

		if (exchange.toString().equals("BITTREX") || exchange.toString().equals("PERSONAL WALLET")) {
			String tradeable = coinDTO.getTradeablePair();
			String symbol = coinDTO.getSymbol();
			String tradePair = tradeable + "-" + symbol;

			if (bittrexData != null) {

				for (int i = 0; i < bittrexData.size(); i++) {
					if (tradePair.equals(bittrexData.get(i).getMarketName())) {

						price = bittrexData.get(i).getLast();

						return price;
					}

				}
			}

		}

		if (exchange.equals("BINANCE")) {
			String symbol = coinDTO.getSymbol();
			String tradeable = coinDTO.getTradeablePair();
			String tradePair = symbol + tradeable;

			if (binanceData != null) {
				for (int i = 0; i < binanceData.size(); i++) {

					if (tradePair.equals(binanceData.get(i).getSymbol())) {
						price = binanceData.get(i).getPrice();

						return price;
					}
				}
			}
		}
		return price;
	}

}
