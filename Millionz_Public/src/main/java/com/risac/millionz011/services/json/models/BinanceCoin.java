package com.risac.millionz011.services.json.models;

import java.util.Objects;

/**
 * Class BinanceCoinSymbol is the model used for storing symbols contained in
 * list in BinanceSymbolJson.
 */
public class BinanceCoin {
	private String symbol;
	private String baseAsset;
	private String quoteAsset;

	public BinanceCoin(String symbol, String baseAsset, String quoteAsset) {
		super();
		this.symbol = symbol;
		this.baseAsset = baseAsset;
		this.quoteAsset = quoteAsset;
	}

	public String getPair() {
		return symbol;
	}

	public void setPair(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return baseAsset.toUpperCase();
	}

	public void setSymbol(String baseAsset) {
		this.baseAsset = baseAsset;
	}

	public String getTradeAsset() {
		return quoteAsset.toUpperCase();
	}

	public void setTradeAsset(String quoteAsset) {
		this.quoteAsset = quoteAsset;
	}

	@Override
	public int hashCode() {
		return Objects.hash(symbol);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BinanceCoin other = (BinanceCoin) obj;
		return Objects.equals(baseAsset, other.baseAsset) && Objects.equals(quoteAsset, other.quoteAsset)
				&& Objects.equals(symbol, other.symbol);
	}

	@Override
	public String toString() {
		return "BinanceCoinSymbolsList [pair=" + symbol + ", symbol=" + baseAsset + ", tradeAsset=" + quoteAsset + "]";
	}

}
