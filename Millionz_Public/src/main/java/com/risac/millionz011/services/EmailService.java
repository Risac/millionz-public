package com.risac.millionz011.services;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.risac.millionz011.entities.AlertEntity;

public class EmailService {

	public String setMessage(AlertEntity alertEntity) {

		String add = "";
		String message = "Dear User," + "\n\nThe price of Your coin " + alertEntity.getCoin().getName()
				+ " on exchange " + alertEntity.getExchange().getName() + " is";
		System.err.println(alertEntity.getMoreThan());
		if (alertEntity.getMoreThan()) {
			add = " more than ";
		} else if (!alertEntity.getMoreThan()) {
			add = " less than ";
		}

		message = message + add + alertEntity.getPrice() + " USDT.";

		System.err.println("message is " + message);
		return message;
	}

	public void sendMail(String setMessage) {

		System.err.println("i sohuld send email");

		final String username = "SENDER EMAIL HERE";
		final String password = "SENDER EMAIL PASS HERE";

		Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true"); // TLS

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("risac79.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("RECIPIENT EMAILS HERE"));
			message.setSubject("Millionz App");
			message.setText(setMessage);

			Transport.send(message);

			System.out.println("Alert email sent.");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}