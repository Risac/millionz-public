package com.risac.millionz011.services.json;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.risac.millionz011.services.json.models.BinanceCoin;

/**
 * Class BinanceSymbolJson is the model used for processing Binance API JSON
 * (symbols) fields.
 */
public class BinanceCoinsJson {

	private String timezone;
	private long serverTime;
	private String[] exhangeFilters;
	private List<BinanceCoin> symbols;

	public BinanceCoinsJson(String timezone, long serverTime, String[] exhangeFilters,
			List<BinanceCoin> symbols) {
		super();
		this.timezone = timezone;
		this.serverTime = serverTime;

		this.exhangeFilters = exhangeFilters;
		this.symbols = symbols;
	}

	public String getTimezone() {
		return timezone;
	}

	public String[] getExhangeFilters() {
		return exhangeFilters;
	}

	public List<BinanceCoin> getSymbols() {
		return symbols;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public void setServerTime(int serverTime) {
		this.serverTime = serverTime;
	}

	public void setExhangeFilters(String[] exhangeFilters) {
		this.exhangeFilters = exhangeFilters;
	}

	public void setSymbols(List<BinanceCoin> symbols) {
		this.symbols = symbols;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(exhangeFilters);
		result = prime * result + Objects.hash(serverTime, symbols, timezone);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BinanceCoinsJson other = (BinanceCoinsJson) obj;
		return Arrays.equals(exhangeFilters, other.exhangeFilters) && serverTime == other.serverTime
				&& Objects.equals(symbols, other.symbols) && Objects.equals(timezone, other.timezone);
	}

	@Override
	public String toString() {
		return "BinanceCoinSymbols [timezone=" + timezone + ", serverTime=" + serverTime + ", exhangeFilters="
				+ Arrays.toString(exhangeFilters) + ", symbols=" + symbols + "]";
	}

}