package com.risac.millionz011.services.json;

import java.util.List;

import com.risac.millionz011.services.json.models.BittrexCoinPrice;

/**
 * Class BittrexTickerJson is the model used for processing Bittrex API (ticker)
 * JSON.
 */
public class BittrexPriceTickerJson {

	private boolean success;
	private String message;

	private List<BittrexCoinPrice> result;

	public BittrexPriceTickerJson(boolean success, String message, List<BittrexCoinPrice> result) {
		super();
		this.success = success;
		this.message = message;
		this.result = result;
	}

	public List<BittrexCoinPrice> getResult() {
		return result;
	}

	public void setResult(List<BittrexCoinPrice> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "BittrexPrevious [result=" + result + "]";
	}

}
