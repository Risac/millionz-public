package com.risac.millionz011.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.risac.millionz011.services.json.BinanceCoinsJson;
import com.risac.millionz011.services.json.BittrexCoinsJson;
import com.risac.millionz011.services.json.models.BinanceCoin;
import com.risac.millionz011.services.json.models.BittrexCoin;

public class CoinsDataService {

	/*
	 * Collect List of BinanceCoins Binance exchange.
	 */
	public List<BinanceCoin> getBinanceCoins() {

		String URL = "https://api.binance.com/api/v3/exchangeInfo";
		List<BinanceCoinsJson> coins = null;
		List<BinanceCoin> results = new ArrayList<>();

		Gson gson = new Gson();

		// Use try and catch to avoid the exceptions
		try {
			URL url = new URL(URL); // creating a url object
			URLConnection urlConnection = url.openConnection(); // creating a urlconnection object
			// wrapping the urlconnection in a bufferedreader
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			// populate Array with Strings
			coins = Arrays.asList(gson.fromJson(bufferedReader, BinanceCoinsJson.class));

			for (BinanceCoinsJson list : coins) {
				results = list.getSymbols();

			}

			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;

	}

	/*
	 * Collect Set of BittrexCoinSymbols from Bittrex exchange.
	 */
	public List<BittrexCoin> getBittrexCoins() {

		String URL = "https://api.bittrex.com/api/v1.1/public/getmarkets";
		List<BittrexCoinsJson> coins = null;

		List<BittrexCoin> results = new ArrayList<>();

		Gson gson = new Gson();

		try {
			URL url = new URL(URL);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			coins = Arrays.asList(gson.fromJson(bufferedReader, BittrexCoinsJson.class));

			for (BittrexCoinsJson sym : coins) {
				results = sym.getResult();
			}

			bufferedReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}

//	/**
//	 * Populate List with symbols from Bittrex exchange
//	 */
//	public List<String> findSymbols() {
//		// String exchange = "BITTREX";
//
//		Exchange exchange = new Exchange();
//		exchange.setName("BITTREX");
//
//		List<String> symbols = new ArrayList<>();
//		List<ExchangeSymbols> findByExchange = symbolServ.findByExchange(exchange);
//
//		for (ExchangeSymbols sym : findByExchange) {
//			symbols.add(sym.getSymbol());
//		}
//
//		return symbols;
//	}

}
