package com.risac.millionz011.services.json.models;

import java.util.Objects;

/**
 * Class BittrexCoinSymbol is the model used for storing symbols contained in
 * list in BittrexSymbolJson.
 */
public class BittrexCoin {

	private String MarketCurrency;
	private String BaseCurrency;
	private String MarketCurrencyLong;
	private String MarketName;

	public BittrexCoin(String marketCurrency, String baseCurrency, String marketCurrencyLong, String marketName) {
		super();
		this.MarketCurrency = marketCurrency;
		this.BaseCurrency = baseCurrency;
		this.MarketCurrencyLong = marketCurrencyLong;
		this.MarketName = marketName;
	}

	public String getSymbol() {
		return MarketCurrency.toUpperCase();
	}

	public void setSymbol(String marketCurrency) {
		this.MarketCurrency = marketCurrency;
	}

	public String getTradeAsset() {
		return BaseCurrency.toUpperCase();
	}

	public void setTradeAsset(String baseCurrency) {
		this.BaseCurrency = baseCurrency;
	}

	public String getName() {
		return MarketCurrencyLong.toUpperCase();
	}

	public void setName(String marketCurrencyLong) {
		this.MarketCurrencyLong = marketCurrencyLong;
	}

	public String getPair() {
		return MarketName.toUpperCase();
	}

	public void setPair(String marketName) {
		this.MarketName = marketName;
	}

//	@Override
//	public int hashCode() {
//		return Objects.hash(BaseCurrency, MarketCurrency, MarketCurrencyLong, MarketName);
//	}

	@Override
	public int hashCode() {
		return Objects.hash(MarketCurrency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BittrexCoin other = (BittrexCoin) obj;
		return Objects.equals(BaseCurrency, other.BaseCurrency) && Objects.equals(MarketCurrency, other.MarketCurrency)
				&& Objects.equals(MarketCurrencyLong, other.MarketCurrencyLong)
				&& Objects.equals(MarketName, other.MarketName);
	}

	@Override
	public String toString() {
		return "BittrexCoin [MarketCurrency=" + MarketCurrency + ", BaseCurrency=" + BaseCurrency
				+ ", MarketCurrencyLong=" + MarketCurrencyLong + ", MarketName=" + MarketName + "]";
	}

}
