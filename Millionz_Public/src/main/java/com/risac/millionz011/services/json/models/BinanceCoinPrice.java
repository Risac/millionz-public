package com.risac.millionz011.services.json.models;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class BinanceCoin is the model used for individual coins stored in
 * BinanceTickerJson.
 */
public class BinanceCoinPrice {

	private String symbol;
	private BigDecimal price;
	private String tradeablePair;

	public BinanceCoinPrice(String symbol, BigDecimal price, String tradeablePair) {
		super();
		this.symbol = symbol;
		this.price = price;
		this.tradeablePair = tradeablePair;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getTradeablePair() {
		return tradeablePair;
	}

	public void setTradeablePair(String tradeablePair) {
		this.tradeablePair = tradeablePair;
	}

	@Override
	public int hashCode() {
		return Objects.hash(price, symbol, tradeablePair);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BinanceCoinPrice other = (BinanceCoinPrice) obj;
		return Objects.equals(price, other.price) && Objects.equals(symbol, other.symbol)
				&& Objects.equals(tradeablePair, other.tradeablePair);
	}

	@Override
	public String toString() {
		return "BinanceCoin [symbol=" + symbol + ", price=" + price + ", tradeablePair=" + tradeablePair + "]";
	}

}
