package com.risac.millionz011.services.json.models;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

/**
 * Class BinanceCoin is the model used for individual coins stored in
 * BittrexTickerJson.
 */
public class BittrexCoinPrice {

	private String MarketName;
	private BigDecimal Last;
	private BigDecimal PrevDay;
	private String lastString;

	public BittrexCoinPrice() {
	}

	public BittrexCoinPrice(String marketName, BigDecimal last, BigDecimal prevDay) {
		super();
		MarketName = marketName;
		Last = last;
		PrevDay = prevDay;
	}

	public String getMarketName() {
		return MarketName;
	}

	public void setMarketName(String marketName) {
		MarketName = marketName;
	}

	public BigDecimal getLast() {
		return Last;
	}

	public String getLastString() {

		String formatted = null;
		DecimalFormat df = new DecimalFormat("#,##0.00");

		if (getLast() != null) {
			double formattedLast = Last.doubleValue();

			formatted = df.format(formattedLast);

			return formatted;
		}
		return null;
	}

	public void setLastString(BigDecimal Last) {

	}

	public void setLast(BigDecimal last) {
		Last = last;
	}

	public BigDecimal getPrevDay() {
		return PrevDay;
	}

	public void setPrevDay(BigDecimal prevDay) {
		PrevDay = prevDay;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Last, MarketName, PrevDay, lastString);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BittrexCoinPrice other = (BittrexCoinPrice) obj;
		return Objects.equals(Last, other.Last) && Objects.equals(MarketName, other.MarketName)
				&& Objects.equals(PrevDay, other.PrevDay) && Objects.equals(lastString, other.lastString);
	}

	@Override
	public String toString() {
		return "BittrexCoin [MarketName=" + MarketName + ", Last=" + Last + ", PrevDay=" + PrevDay + ", lastString="
				+ lastString + "]";
	}

}
