# MillionzApp

## Description
Crypto portfolio Web application.

## Getting started

Before first start:


Please provide your settings for MySQL base.
![settings](/uploads/2bad25779c9b85eb7e6cb14cf1224bae/settings.jpg)


You can find MySQL settings in application.properties and
Hibernate will provide necessary tables for you!


For Alerts, please provide details for email sender and recipient.
![settings_email](/uploads/2930403b8bd08ef085c566b380e8905c/settings_email.jpg)



You can find method sendMail() in /MillionzApp/src/main/java/com/risac/millionz011/services/EmailService.java

After first start:

Application is deployed on localhost:8080




## Features
- Portfolio page, featuring list of owned assets. Add assets, edit or delete as your heart desires! Want to know how much you will profit if BTC climbs to the moon? Enter TARGET SELL PRICE and find out!

![portfolio](/uploads/0bcc7d42ca683990507eb02f139273c0/portfolio.jpg)

- Crypto Calculator for quick checks of crypto prices, based on exchange APIs. Each exchange holds its own set of crypto coins and fiat pairs.

![calculator](/uploads/87ce127c32a209f845328c794eb225fe/calculator.jpg)

- Set alerts which are dynamically checked.

![alerts](/uploads/337dd09c273a8217d0c23442aed6e686/alerts.jpg)


- Chyron! Flashy newsreel marquee (IN TECHNICOLOR!) on every web page, convinienty changing colors based on last 24 price change. Assets carefully selected by yours truly.

![chyron](/uploads/b17f8cd57ca553dc3b7105753a206020/chyron.jpg)

- See horrendous logo! ![millionz_logo](/uploads/ffbbb98664ac4d8f607d14d7c8ab9a96/millionz_logo.jpg)





## Usage
May the Force be with you in your endeavours in cryptocurrency world, because I won't be!
Whatever you do, you're doing it yourself and I won't be hold accountable for your actions!

## Support
You can reach me on risac79@gmail.com or damirmiklos@gmail.com

## Roadmap
This is a beginner's projet, with all it's ups, downs and down-belows. I'm doing it for fun and to learn something from it.

## Contributing
Feel free to hit me up on email if you have any comments, suggestions or whatevers.

## Author
Risac

## Project status
Work in progress.

